const btncalcular = document.getElementById ('btncalcular');
const btnlimpiar = document.getElementById ('btnlimpiar');

btncalcular.addEventListener ('click',function calcular ()


{
    const opcion = document.getElementById ('idOpcion').value;
    const idNum1 = document.getElementById ('idNum1').value;
    const idNum2 = document.getElementById ('idNum2').value;
    const txtRes = document.getElementById ('idResult');

    let res = 0;
    let Num1 = parseFloat (idNum1);
    let Num2 = parseFloat (idNum2);
    let opc = parseFloat (opcion);

    switch (opc)
    {
        case 1: res = Num1+Num2; break;
        case 2: res = Num1-Num2; break;
        case 3: res = Num1*Num2; break;
        case 4: res = Num1/Num2; break;
    }
    txtRes.value = res;

});



btnlimpiar.addEventListener('click', limpiar);

function limpiar()
{
     document.getElementById ('idOpcion').value = 1;
    document.getElementById ('idNum1').value ='';
     document.getElementById ('idNum2').value ='';
     document.getElementById ('idResult').value ='';
}